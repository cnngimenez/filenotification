package cnngimenez.gitlab.io.filenotification.ui.urls

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import cnngimenez.gitlab.io.filenotification.R
import cnngimenez.gitlab.io.filenotification.databinding.FragmentUrlsBinding
import cnngimenez.gitlab.io.filenotification.notifier.URLRegister
import cnngimenez.gitlab.io.filenotification.notifier.URLRepository
import java.io.File

class URLFragment : Fragment() {

    private val logTag = "URLFragment"
    private var _binding: FragmentUrlsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var urlRepo: URLRepository? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUrlsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        /*
        // URLViewModel is not used!

        val URLViewModel =
            ViewModelProvider(this).get(URLViewModel::class.java)

        val textView: TextView = binding.txtUrl
        URLViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }
        */

        urlRepo = URLRepository(File(requireContext().filesDir,
                                     getString(R.string.urlrepo_filename)).absolutePath)

        assignHandlers()
        updateURLs()

        return root
    }

    private fun addCard(urlRegister: URLRegister) {
        val card = CardView(this.requireContext())
        card.apply {
            radius = 8F
            cardElevation = 8.0F
            maxCardElevation = 10.0F
            preventCornerOverlap = true
            useCompatPadding = true
            addView(LinearLayout(context).apply {
                this.orientation = LinearLayout.VERTICAL

                addView(TextView(context).apply {
                    this.text = urlRegister.name
                    compoundDrawablePadding = 2
                })
                addView(TextView(context).apply {
                    this.text = urlRegister.url.toString()
                    compoundDrawablePadding = 2
                })
            })
        }
        binding.llCards.addView(card)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun onAddUrlClick() {
        val url = binding.txtUrl.text.toString()
        val name = binding.txtName.text.toString()
        urlRepo!!.add(name, url)
        updateURLs()
    }

    private fun updateURLs() {
        Log.i(logTag, "Updating URLs")

        binding.llCards.removeAllViews()
        urlRepo!!.urls.forEach {
            addCard(it)
        }
    }

    private fun assignHandlers() {
        binding.btnAddUrl.setOnClickListener {
            onAddUrlClick()
        }
        binding.btnClear.setOnClickListener {
            onClearClick()
        }
    }

    private fun onClearClick() {
        urlRepo!!.clear()
        updateURLs()
    }
}