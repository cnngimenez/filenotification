package cnngimenez.gitlab.io.filenotification.ui.notifications

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import cnngimenez.gitlab.io.filenotification.R
import cnngimenez.gitlab.io.filenotification.databinding.FragmentNotificationsBinding
import cnngimenez.gitlab.io.filenotification.notifier.Notification
import cnngimenez.gitlab.io.filenotification.notifier.NotificationFile
import cnngimenez.gitlab.io.filenotification.notifier.URLRepository
import java.io.File

class NotificationsFragment : Fragment() {

    private val logTag = "NotificationFragment"
    private var _binding: FragmentNotificationsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var curFile: Int = 0
    private var urlRepository: URLRepository? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        /*val notificationsViewModel =
            ViewModelProvider(this).get(NotificationsViewModel::class.java)*/

        _binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        val root: View = binding.root

        curFile = 0
        urlRepository = URLRepository(
            File(requireContext().filesDir, getString(R.string.urlrepo_filename)).absolutePath)

        /*val textView: TextView = binding.textGallery
        notificationsViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }*/

        assignHandlers()
        update()

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun assignHandlers() {
        binding.btnPrev.setOnClickListener() {
            if (curFile > 0)
                curFile--
            else
                curFile = urlRepository!!.urls.lastIndex

            update()
        }
        binding.btnNext.setOnClickListener() {
            if (curFile < urlRepository!!.urls.lastIndex)
                curFile++
            else
                curFile = 0
            update()
        }
    }

    private fun update() {
        val urlRegister = urlRepository!![curFile] ?: return

        binding.txtCurfile.text = urlRegister.name

        val notificationFile = NotificationFile(File(requireContext().filesDir, "$curFile.txt").absolutePath)
        Log.i(logTag, "Opening notification file: ${notificationFile.getPath()}")
        val notificationList = notificationFile.getAllNotifications()

        binding.llNotifs.removeAllViews()
        if (notificationList.isEmpty())
            createEmptyCard()
        else
            populateNotifications(notificationList)

    }

    private fun createEmptyCard() {
        binding.llNotifs.addView(CardView(requireContext()).apply {
            radius = 8F
            cardElevation = 8.0F
            maxCardElevation = 10.0F
            preventCornerOverlap = true
            useCompatPadding = true
            addView(TextView(context).apply {
                text = getString(R.string.no_notifications)
                compoundDrawablePadding = 2
            })
        })
    }

    private fun populateNotifications(notificationList: List<Notification>) {
        notificationList.forEach {
            Log.i(logTag, "Showing notification card: ${it.toString()}")
            if (it.valid)
                binding.llNotifs.addView(createCard(it))
        }
    }


    private fun createCard(notif: Notification) : CardView {
        return CardView(requireContext()).apply {
            radius = 8F
            cardElevation = 8.0F
            maxCardElevation = 10.0F
            preventCornerOverlap = true
            useCompatPadding = true
            addView(LinearLayout(context).apply {
                this.orientation = LinearLayout.VERTICAL

                addView(TextView(context).apply {
                    this.text = notif.toReadableString()
                    compoundDrawablePadding = 2
                })
            })
        }
    }
}