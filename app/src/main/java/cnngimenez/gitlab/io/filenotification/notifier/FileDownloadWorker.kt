package cnngimenez.gitlab.io.filenotification.notifier

import android.content.Context
import android.os.Build
// import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.work.Worker
import androidx.work.WorkerParameters
import cnngimenez.gitlab.io.filenotification.R
import java.io.File
import java.util.*

class FileDownloadWorker(appContext: Context, workerParams: WorkerParameters)
    : Worker(appContext, workerParams)  {

    // private val logTag = "FileDownloadWorker"

    private val fileDownloader = FileDownloader(
            Notifier(applicationContext),
            URLRepository(File(
                applicationContext.filesDir,
                applicationContext.getString(R.string.urlrepo_filename)
            ).absolutePath),
            applicationContext.filesDir.absolutePath)

    @RequiresApi(Build.VERSION_CODES.O)
    override fun doWork(): Result {
        fileDownloader.downloadAndNotify()

        // And now, notify if there were errors...
        /*if (fileDownloader.withErrors())
            Toast.makeText(applicationContext, fileDownloader.lastErrors(), Toast.LENGTH_SHORT).show()*/

        registerLastExecution()

        return Result.success()
    }

    private fun registerLastExecution() {
        val nowDate = Date().toString()
        val sharedPref = applicationContext.getSharedPreferences(
                applicationContext.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )

        with (sharedPref.edit()) {
            putString("lastWorkerExecution", nowDate)
            putString("lastWorkerError", fileDownloader.lastErrors())
            apply()
        }
    }

}