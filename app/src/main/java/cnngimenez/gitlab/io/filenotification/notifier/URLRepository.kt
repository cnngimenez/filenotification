package cnngimenez.gitlab.io.filenotification.notifier

import android.os.Build
import androidx.annotation.RequiresApi
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class URLRepository {
    var urls : MutableList<URLRegister> = mutableListOf()
    private val filepath:  String

    constructor(filepath: String) {
        this.filepath = filepath
        reload()
    }

    fun add(url: URLRegister) {
        urls.add(url)
        save()
    }

    fun add(name: String, url: String) {
        urls.add(URLRegister(name, url))
        save()
    }

    operator fun get(index: Int) : URLRegister? {
        return if (index >=0 && index < urls.size) {
            urls[index]
        } else {
            null
        }
    }

    fun get(name: String) : URLRegister? {
        return urls.find {
            it.name == name
        }
    }

    fun delete(index: Int) {
        urls.removeAt(index)
        save()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun delete(name: String) {
        urls.removeIf { it.name == name }
        save()
    }

    /**
     * Load all the URLs from a file.
     */
    fun reload() {
        if (!File(filepath).exists() || !File(filepath).canRead()) {
            return
        }
        with (File(filepath).bufferedReader()) {
            urls = readLines().map { URLRegister(it) }.toMutableList()
            close()
        }
    }

    /**
     * Save all the URLs into a file.
     */
    fun save() {
        with (File(filepath).bufferedWriter()) {
            urls.forEach {
                write(it.toSaveString() + "\n")
            }
            close()
        }
    }

    fun clear() {
        urls.clear()
        save()
    }

    /**
     * Download the file from the URL id into a local file.
     *
     * Use the URL pointed by the given id and download the file into the local file. The local
     * file will be indicated by fileName path.
     *
     * @param id Which URL to download.
     * @param fileName the path to where to download the file.
     *
     * @throws java.io.FileNotFoundException when the URL could not be downloaded.
     */
    @RequiresApi(Build.VERSION_CODES.O)
    fun download(id : Int, fileName: String) {
        if (id !in urls.indices) return

        val urlReg = urls[id]
        urlReg.url.openStream().use {
            Files.copy(it, Paths.get(fileName))
        }
    }
}