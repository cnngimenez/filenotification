package cnngimenez.gitlab.io.filenotification.ui.home

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import cnngimenez.gitlab.io.filenotification.R
import cnngimenez.gitlab.io.filenotification.SettingsActivity
import cnngimenez.gitlab.io.filenotification.databinding.FragmentHomeBinding
import cnngimenez.gitlab.io.filenotification.notifier.Notifier


class HomeFragment : Fragment() {

    private val logTag = "HomeFragment"
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private var notifier: Notifier? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        notifier = Notifier(requireContext())

        /*
        val homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        val textView: TextView = binding.txtLastnotification
        homeViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }
        */

        assignHandlers()
        update()

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun update() {
        if (notifier!!.getLastNotification().valid) {
            binding.txtLastnotification.text = notifier!!.getLastNotification().toReadableString()
        } else {
            binding.txtLastnotification.text = getString(R.string.txt_lastnotif_nonotif_label)
        }

        updateLastWorkerTime()
        updateStatus()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun updateStatus(solve: Boolean = false) {
        //TODO("check for auto-start in some devices, see https://stackoverflow.com/questions/44383983/how-to-programmatically-enable-auto-start-and-floating-window-permissions")

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.requireContext())
        val runWorker = sharedPreferences.getBoolean("settings_run_worker", true)

        var txt = getString(R.string.status_title)
        val pm = requireContext().getSystemService(Context.POWER_SERVICE) as PowerManager
        if (!pm.isIgnoringBatteryOptimizations(requireContext().packageName)){
            if (solve) showOptimizationDialog()
            Log.w(logTag, "Battery optimizations are not being ignored for ${requireContext().packageName}.")
            txt += getString(R.string.status_battery_optimizations_not_ignored)
        }

        if (!runWorker) {
            if (solve) showSettingsDialog()
            txt += getString(R.string.status_worker_not_running)

        }

        binding.txtStatus.text = txt
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun showOptimizationDialog() {
        val alertDialog: AlertDialog? = activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.apply {
                setTitle(getString(R.string.alert_optimization_dialog_title))
                setMessage(getString(R.string.alert_optimization_dialog_message))
                setPositiveButton(getString(R.string.alert_dialog_open)) { _, _ ->
                    val intent = Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS)
                    startActivity(intent)
                }
                setNegativeButton(getString(R.string.alert_dialog_cancel)) { _, _ -> }
            }
            builder.create()
        }
        alertDialog!!.show()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun showSettingsDialog() {
        val alertDialog: AlertDialog? = activity?.let { fragActivity ->
            val builder = AlertDialog.Builder(fragActivity)
            builder.apply {
                setTitle(getString(R.string.alert_worker_dialog_title))
                setMessage(getString(R.string.alert_worker_dialog_message))
                setPositiveButton(getString(R.string.alert_dialog_open)) { _, _ ->
                        Intent(this.context, SettingsActivity::class.java).also {
                            startActivity(it)
                        }
                    }
                setNegativeButton(getString(R.string.alert_dialog_cancel)) { _, _ -> }
            }
            builder.create()
        }
        alertDialog!!.show()
    }


    private fun updateLastWorkerTime() {
        val sharedPref = requireContext().getSharedPreferences(
            getString(R.string.preference_file_key),
            Context.MODE_PRIVATE
        )

        val lastTime = sharedPref.getString("lastWorkerExecution", getString(R.string.status_worker_no_executed))
        val lastError = sharedPref.getString("lastWorkerError", getString(R.string.status_worker_no_error))
        binding.txtLastworkertime.text = "$lastTime\n$lastError"
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun assignHandlers() {
        binding.btnClearnotif.setOnClickListener() {
            notifier!!.clearLastNotification()
            update()
        }
        binding.btnSolve.setOnClickListener() {
            updateStatus(true)
        }
    }

}