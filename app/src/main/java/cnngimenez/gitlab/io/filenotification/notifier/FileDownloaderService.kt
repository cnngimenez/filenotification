package cnngimenez.gitlab.io.filenotification.notifier

import android.app.Service
import android.content.Intent
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import cnngimenez.gitlab.io.filenotification.R
import java.io.File
import java.util.*

class FileDownloaderService : Service() {

    private val logTag = "FileDownloaderService"
    private var fileDownloader: FileDownloader? = null
    private var urlRepo: URLRepository? = null

    private var serviceLooper: Looper? = null
    private var serviceHandler: ServiceHandler? = null

    private inner class ServiceHandler(looper: Looper) : Handler(looper) {

        @RequiresApi(Build.VERSION_CODES.O)
        override fun handleMessage(msg: Message) {
            try {

                fileDownloader!!.downloadAndNotify()

                // And now, notify if there were errors...
                if (fileDownloader!!.withErrors()) {
                    Toast.makeText(applicationContext, fileDownloader!!.lastErrors(), Toast.LENGTH_SHORT).show()
                }

            } catch (e: InterruptedException) {
                // Restore interrupt status.
                Thread.currentThread().interrupt()
            }

            stopSelf(msg.arg1)
        }
    }

    override fun onCreate() {
        super.onCreate()

        HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND).apply {
            start()

            serviceLooper = looper
            serviceHandler = ServiceHandler(looper)
        }

        val notifier = Notifier(applicationContext)
        notifier.createNotificationChannel()

        urlRepo = URLRepository(File(applicationContext.filesDir,
                                     getString(R.string.urlrepo_filename)).absolutePath)
        fileDownloader = FileDownloader(notifier, urlRepo!!, applicationContext.filesDir.absolutePath)

    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        Log.i(logTag, "Starting service: Downloading")

        doDownload(startId)

        return START_STICKY
    }

    private fun doDownload(startId : Int) {
        serviceHandler?.obtainMessage()?.also { msg ->
            msg.arg1 = startId
            serviceHandler?.sendMessage(msg)
        }
    }

    /*
    fun notifyNow() {
        val lastNotification = notifier!!.getLastNotification()
        val lastTimestamp = if (lastNotification.valid)
                                lastNotification.creationTime
                            else
                                Date(0)
        val notification = notificationFile!!.getNextNotification(lastTimestamp)

        Log.i(logTag, "Notifying: $notification")

        if (!notification.valid) {
            Toast.makeText(
                applicationContext!!,
                "No *new* notification found.",
                Toast.LENGTH_SHORT
            ).show()
            return
        }

        notifier!!.notifyNow(notification)
    }

    private fun getNotificationFilePath() : String {
        val fileDir : File = applicationContext.filesDir
        Log.i(logTag, "Cache directory is: $fileDir")
        Log.i(logTag, "Cache directory exists? ${fileDir.exists()}")
        Log.i(logTag, "Cache directory is writable? ${fileDir.canWrite()}")

        return File(fileDir.toString(), "notifications.txt").toString()
    }
     */
}