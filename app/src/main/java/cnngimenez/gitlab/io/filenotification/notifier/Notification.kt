package cnngimenez.gitlab.io.filenotification.notifier

import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * An internal representation of a Notification.
 */
class Notification {

    private val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.UK)
    private val logTag = "Notification"

    var creationTime: Date = Date(0)
    var text: String = ""
    var valid: Boolean = false

    /**
     * Create an empty notification with an "invalid" or zeroed date.
     */
    constructor()

    constructor(timestamp: String, text: String) {
        valid = true
        creationTime = parseTimestamp(timestamp)
        this.text = text
    }

    constructor(line: String) {
        valid = true
        creationTime = parseLineTimestamp(line)
        text = parseLineText(line)
    }

    fun creationTimeString() : String {
        return dateToTimestamp(creationTime)
    }

    /**
     * Parse the string and return a date.
     *
     * @return null if the string could not be parsed.
     */
    private fun parseTimestamp(timestamp : String) : Date {
        return try {
            df.parse(timestamp) ?: Date(0)
        } catch (ex : ParseException) {
            // String could not be parsed
            Log.w(logTag, "Timestamp could not be parsed.")
            valid = false
            Date(0)
        }
    }

    private fun parseLineTimestamp(line: String) : Date {
        val arr = line.split(",")
        return parseTimestamp(arr.first())
    }

    private fun parseLineText(line: String) : String {
        return line.substringAfter(",")
    }

    private fun dateToTimestamp(date : Date) : String {
        return df.format(date)
    }

    override fun toString(): String {
        return "${creationTimeString()},\"$text\""
    }

    fun toReadableString(): String {
        return "$creationTime\n$text"
    }
}