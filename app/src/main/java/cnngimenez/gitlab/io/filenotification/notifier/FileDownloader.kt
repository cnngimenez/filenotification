package cnngimenez.gitlab.io.filenotification.notifier

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import java.io.File
import java.io.FileNotFoundException
import java.util.*

class FileDownloader(private val notifier: Notifier,
                     private val urlRepo: URLRepository,
                     private val dirPath: String){

    private val logTag = "FileDownloaderService"
    private var lastErrors: String = ""

    @RequiresApi(Build.VERSION_CODES.O)
    fun downloadAndNotify() {
        lastErrors = ""

        downloadAll()
        notifyNow()
    }

    /**
     * There was any error when downloading or notifying?
     *
     * @return True if there were errors. Check lastErrors property.
     */
    fun withErrors() : Boolean {
        return lastErrors != ""
    }

    fun lastErrors() : String {
        return lastErrors
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun downloadAll() {
        for (i in urlRepo.urls.indices) {
            try {
                val notificationFile = getNotificationFile(i)
                notificationFile.prepareForDownload()
                urlRepo.download(i, notificationFile.getPath())
            } catch (e: FileNotFoundException) {
                Log.w(logTag, "Could not download ${urlRepo.urls[i].url}")
                lastErrors += "Could not download from ${urlRepo.urls[i].name}\n"
            }
        }
    }

    private fun notifyNow() {
        val lastNotification = notifier.getLastNotification()
        val lastTimestamp = if (lastNotification.valid)
            lastNotification.creationTime
        else
            Date(0)

        for (i in urlRepo.urls.indices) {
            val notificationFile = getNotificationFile(i)
            val notification = notificationFile.getNextNotification(lastTimestamp)

            Log.i(logTag, "Notifying: $notification")

            notifier.notifyNow(notification)
        }
    }

    /**
     * Return the NotificationFile from the URL id.
     *
     * @param id Is the urlRepo id.
     */
    private fun getNotificationFile(id: Int) : NotificationFile {
        return NotificationFile (getNotificationFilePath(id))
    }

    private fun getNotificationFilePath(id: Int) : String {
        return File(dirPath, "$id.txt").absolutePath
    }

}