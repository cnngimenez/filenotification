package cnngimenez.gitlab.io.filenotification.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cnngimenez.gitlab.io.filenotification.notifier.Notifier

class HomeViewModel : ViewModel() {

    private var notifier: Notifier? = null

    fun setNotificationFile(notifier: Notifier) {
        this.notifier = notifier
    }

    private val _text = MutableLiveData<String>().apply {
        value = notifier!!.getLastNotification().toString()
    }

    val text: LiveData<String> = _text

}