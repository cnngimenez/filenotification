package cnngimenez.gitlab.io.filenotification.notifier

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import cnngimenez.gitlab.io.filenotification.notifier.Notification as NotifierNotification
import cnngimenez.gitlab.io.filenotification.R

class Notifier(applicationContext: Context) {
    private val logTag = "Notifier"
    private val channelID = "File Notification"
    private var notificationId = 0
    private val context: Context

    init {
        context = applicationContext
    }

    fun getLastNotification() : NotifierNotification {
        val sharedPref = context
            .getSharedPreferences(
                context.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )

        val timestamp = sharedPref.getString("lastNotification", "") ?: return NotifierNotification()
        return NotifierNotification(timestamp)
    }

    fun clearLastNotification() {
        val sharedPref = context
            .getSharedPreferences(
                context.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE
            )

        with (sharedPref.edit()) {
            putString("lastNotification", null)
            apply()
        }
    }

    /**
     * Save the last timestamp.
     *
     * @param notification The Notification instance to store.
     */
    fun setLastNotification(notification: NotifierNotification) {
        val sharedPref = context
            .getSharedPreferences(context.getString(R.string.preference_file_key),
                Context.MODE_PRIVATE)

        with (sharedPref.edit()) {
            putString("lastNotification", notification.toString())
            apply()
        }
    }

    fun notifyNow(notification: NotifierNotification) {
        if (!notification.valid)
            return

        notificationId += 1
        with (NotificationManagerCompat.from(context)) {
            notify(notificationId, newNotification(notification.text))
        }

        setLastNotification(notification)
    }

    private fun newNotification(text: String) : Notification {
        val builder = NotificationCompat.Builder(context, channelID)
            .setSmallIcon(R.drawable.ic_menu_camera)
            .setContentTitle("File Notification")
            .setContentText(text)
            .setStyle(
                NotificationCompat.BigTextStyle()
                .bigText(text))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        return builder.build()
    }

    fun createNotificationChannel() {
        Log.i(logTag, "Creating notification channel")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(R.string.channel_name)
            val descriptionText = context.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}