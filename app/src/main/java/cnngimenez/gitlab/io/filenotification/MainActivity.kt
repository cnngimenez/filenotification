package cnngimenez.gitlab.io.filenotification

import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.preference.PreferenceManager
import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequest
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import cnngimenez.gitlab.io.filenotification.databinding.ActivityMainBinding
import cnngimenez.gitlab.io.filenotification.notifier.FileDownloadWorker
import cnngimenez.gitlab.io.filenotification.notifier.FileDownloaderService
import com.google.android.material.navigation.NavigationView
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {

    private val logTag = "MainActivity"

    private lateinit var sharedPreferences: SharedPreferences
    /**
     *  Listener used when the preferences changes.
     *
     *  A strong reference is required according to:
     *  https://developer.android.com/develop/ui/views/components/settings/use-saved-values#kotlin
     *
     *  TODO("Move/copy listener to SettingsActivity!")
     *  TODO("I don't see that this listener works... Logcat does not show anything... :/")
     */
    @RequiresApi(Build.VERSION_CODES.M)
    private val preferenceListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        Log.i(logTag, "Settings changed: $key")

        if (key == "settings_run_worker") {
            if (sharedPreferences.getBoolean(key, true)) {
                Log.i(logTag, "Settings changed: re-creating workers.")
                createWorkers()
            } else {
                Log.i(logTag, "Settings changed: removing workers.")
                removeWorkers()
            }
       }
    }

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_notifications, R.id.nav_urls
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.applicationContext)
        val runWorker = sharedPreferences.getBoolean("settings_run_worker", true)

        if (runWorker) {
            createWorkers()
        } else {
            removeWorkers()
        }

        assignHandlers()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun assignHandlers(){
        binding.appBarMain.fab.setOnClickListener {
            /* view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()*/
            Log.i(logTag, "Update clicked")
            Intent(this, FileDownloaderService::class.java).also { intent ->
                startService(intent)
            }
        }
        binding.appBarMain.toolbar.setOnClickListener {
            Log.i(logTag, "toolbar on click listener.")
        }
        binding.appBarMain.toolbar.setOnMenuItemClickListener { menuItem ->
            Log.i(logTag, "Menu item clicked: $menuItem")
            if (menuItem.title == getString(R.string.action_settings)) {
                Intent(this, SettingsActivity::class.java).also {
                    startActivity(it)
                }
            }
            if (menuItem.title == getString(R.string.menu_about)) {
                Intent(this, AboutActivity::class.java).also {
                    startActivity(it)
                }
            }

            return@setOnMenuItemClickListener true
        }

        sharedPreferences.registerOnSharedPreferenceChangeListener(preferenceListener)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        sharedPreferences.registerOnSharedPreferenceChangeListener(preferenceListener)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onPause() {
        super.onPause()
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferenceListener)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun createWorkers() {
        val networkType = if (sharedPreferences.getBoolean("sw_run_metered", false))
            NetworkType.METERED
        else
            NetworkType.UNMETERED
        val idle = sharedPreferences.getBoolean("sw_idle_only", false)
        val batteryNotLow = sharedPreferences.getBoolean("sw_run_batnotlow", false)
        val repeatInterval: Long = try {
            sharedPreferences.getString("lst_time_interval", "15")!!.toLong()
        } catch (e: NumberFormatException) {
            15
        }

        Log.i(logTag, "Creating worker: " +
                "Minimum repeatInterval: ${PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS}, " +
                "minimum flexInterval: ${PeriodicWorkRequest.MIN_PERIODIC_FLEX_MILLIS}" +
        "Network type: $networkType | Idle only: $idle | Battery not low: $batteryNotLow" +
        "Repeat interval: $repeatInterval.")


        val constraints = Constraints.Builder()
            .setRequiredNetworkType(networkType)
            .setRequiresDeviceIdle(idle)
            .setRequiresBatteryNotLow(batteryNotLow)
            .build()

        val fileDownloadRequest =
            PeriodicWorkRequestBuilder<FileDownloadWorker>(
                repeatInterval, TimeUnit.MINUTES,
                repeatInterval, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .build()
        val wm = WorkManager.getInstance(applicationContext)
        wm.cancelAllWork()
        wm.enqueue(fileDownloadRequest)
    }

    private fun removeWorkers() {
        WorkManager.getInstance(applicationContext)
            .cancelAllWork()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}