package cnngimenez.gitlab.io.filenotification.ui.about

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import cnngimenez.gitlab.io.filenotification.R
import cnngimenez.gitlab.io.filenotification.databinding.FragmentAboutProjectBinding

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
/*private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"*/

class AboutProjectFragment : Fragment() {
    /*
    private var param1: String? = null
    private var param2: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }
    */

    private var _binding: FragmentAboutProjectBinding? = null
    private val binding get() = _binding!!

    private var current_position = 0
    private val texts = listOf(
        R.string.txt_about_project,
        R.string.txt_about_licence
    )
    private val images = listOf(
        R.drawable.question_girl,
        R.drawable.gpl_v3_logo_color
    )
    private lateinit var circles: List<ImageView>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        // inflater.inflate(R.layout.fragment_about_project, container, false)
        _binding = FragmentAboutProjectBinding.inflate(inflater, container, false)
        // val root: View = binding.root

        circles = listOf(
            binding.ivStep1,
            binding.ivStep2
        )

        assignHandler()
        update()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    /*
    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AboutProjectFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
    */

    private fun assignHandler() {
        binding.btnAboutNext.setOnClickListener {
            next()
        }
        binding.btnAboutPrev.setOnClickListener {
            previous()
        }
    }

    private fun update() {
        binding.txtText.text = getString(texts[current_position])
        binding.ivImage.setImageResource(images[current_position])
        circles.forEach {
            it.setImageResource(androidx.appcompat.R.drawable.btn_radio_off_mtrl)
        }
        circles[current_position].setImageResource(androidx.appcompat.R.drawable.btn_radio_on_mtrl)
    }

    fun previous() {
        current_position --
        if (current_position < 0) {
            current_position = texts.size - 1
        }
        update()
    }

    fun next() {
        current_position ++
        if (current_position >= texts.size) {
            current_position = 0
        }
        update()
    }
}