package cnngimenez.gitlab.io.filenotification

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cnngimenez.gitlab.io.filenotification.databinding.ActivityAboutBinding

class AboutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAboutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_about)

        binding = ActivityAboutBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

}