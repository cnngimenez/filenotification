package cnngimenez.gitlab.io.filenotification.notifier

import java.net.URL

class URLRegister {
    val name: String
    val url: URL

    constructor(name: String, url: String) {
        this.name = name
        this.url = URL(url)
    }

    /**
     * Parse name and URL from a line.
     *
     * The line separates the name and URL with a comma for example:
     * NAME OF THE URL,http://example.org/file.txt
     */
    constructor(line: String) {
        this.name = line.substringBefore(",")
        this.url = URL(line.substringAfter(","))
    }

    fun toSaveString() : String {
        return "$name,$url"
    }

}