package cnngimenez.gitlab.io.filenotification.notifier

import android.util.Log
import java.io.File
import java.util.Date

class NotificationFile(path: String) {

    private var notificationFile : File? = null
    private val logTag = "NotificationFile"

    init {
        createFile(path)
    }

    fun getPath () : String {
        return notificationFile!!.toString()
    }

    private fun createFile(path: String) {
        /*val filedir : File = context.filesDir
        Log.i(logTag, "Cache directory is: $filedir")
        Log.i(logTag, "Cache directory exists? ${filedir.exists()}")
        Log.i(logTag, "Cache directory is writable? ${filedir.canWrite()}")

        notificationFile = File(filedir.toString(), "notifications.txt")
        Log.i(logTag, "Notification file will be in: $notificationFile")
         */
        notificationFile = File(path)
        Log.i(logTag, "Notification file will be in: $notificationFile")
    }

    /**
     * Prepare the file where the notification is going to be stored.
     */
    fun prepareForDownload() {
        if (notificationFile!!.exists()) {
            notificationFile!!.delete()
            Log.i(logTag, "Notification file exists and was deleted.")
        } else {
            Log.i(logTag, "Notification file does not exists.")
        }
    }

    fun getNextNotification(lastTimestamp: Date) : Notification {
        if (!notificationFile!!.exists()) return Notification()

        var found = false
        val buffer = notificationFile!!.bufferedReader()
        var line = ""
        var notification = Notification()

        Log.i(logTag, "lastTimestamp: $lastTimestamp")

        while (!found && buffer.ready()) {
            line = buffer.readLine()
            notification = Notification(line)
            if (notification.valid &&  notification.creationTime > lastTimestamp) {
                found = true
            }
            Log.i(logTag, "Line: $line, timestamp: ${notification.creationTime}, found: $found")
        }

        if (!found) {
            Log.i(logTag, "Notification not found, returning an invalid one.")
            return Notification()
        }

        Log.i(logTag, "Notification found: $line")
        return notification
    }

    fun getAllNotifications() : List<Notification> {
        if (!notificationFile!!.exists()) return listOf()

        return notificationFile!!.bufferedReader().readLines().map {
            Notification(it)
        }
    }

}